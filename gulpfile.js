var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var pug = require('gulp-pug');

// Gulp Sass Task 
gulp.task('sass', function() {
  gulp.src('./scss/{,*/}*.{scss,sass}')
    .pipe(sourcemaps.init())
    .pipe(sass({
      errLogToConsole: true
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./css'));
});

gulp.task('pug',function(){
	gulp.src('./src/*.pug')
	 .pipe(pug({
	 	pretty:true
	 }))
	 .pipe(gulp.dest('./destination'))
});

gulp.task('watch',function(){
	gulp.watch('./src/*.pug',['pug'])
});

// Create Gulp Default Task
// ------------------------
// Having watch within the task ensures that 'sass' has already ran before watching
// 
// This setup is slightly different from the one on the blog post at
// http://www.zell-weekeat.com/gulp-libsass-with-susy/#comment-1910185635
gulp.task('default', ['sass','pug'], function () {
  gulp.watch('./scss/{,*/}*.{scss,sass}', ['sass'])
});

